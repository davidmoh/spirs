const express = require('express');
const cors = require('cors');
const sqlite3 = require('sqlite3').verbose();
const cron = require('node-cron');
const fs = require('fs');
const ping = require('ping');

// Dynamic import for node-fetch
let fetch;

// Load configuration from JSON file
const config = JSON.parse(fs.readFileSync('config.json', 'utf8'));
const { httpServers, icmpServers, pingInterval } = config;

// Initialize Express app and SQLite DB
const app = express();
const db = new sqlite3.Database('spirs.db');

db.serialize(() => {
  db.run("CREATE TABLE IF NOT EXISTS ping_results (id INTEGER PRIMARY KEY AUTOINCREMENT, server TEXT, check_type TEXT, status TEXT, response_time INTEGER, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)");
});

// Enable CORS for all routes
app.use(cors());

// Function to perform HTTP GET check
const httpCheck = async (server) => {
  if (!fetch) {
    fetch = (await import('node-fetch')).default;
  }

  try {
    const start = new Date().getTime();
    const response = await fetch(server);
    const end = new Date().getTime();

    const status = response.ok ? 'success' : 'error';
    const responseTime = end - start;
    db.run("INSERT INTO ping_results (server, check_type, status, response_time) VALUES (?, ?, ?, ?)", [server, 'http', status, responseTime]);
  } catch (error) {
    db.run("INSERT INTO ping_results (server, check_type, status, response_time) VALUES (?, ?, ?, ?)", [server, 'http', 'error', -1]);
  }
};

// Function to perform ICMP ping check
const icmpCheck = (server) => {
  ping.promise.probe(server)
    .then((res) => {
      const status = res.alive ? 'success' : 'error';
      const responseTime = res.alive ? res.time : -1;
      db.run("INSERT INTO ping_results (server, check_type, status, response_time) VALUES (?, ?, ?, ?)", [server, 'icmp', status, responseTime]);
    });
};

// Function to ping servers
const pingServers = () => {
  httpServers.forEach(server => httpCheck(server));
  icmpServers.forEach(server => icmpCheck(server));
};

// Schedule server pinging
cron.schedule(`*/${pingInterval / 60000} * * * *`, () => {
  console.log('Pinging servers...');
  pingServers();
});

// Endpoint to retrieve ping results
app.get('/results', (req, res) => {
  db.all("SELECT * FROM ping_results", [], (err, rows) => {
    if (err) {
      res.status(500).json({ error: err.message });
      return;
    }
    res.json({ data: rows });
  });
});

// Start the server
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`SPiRS running on port ${PORT}`);
  pingServers();  // Initial ping on server start
});
